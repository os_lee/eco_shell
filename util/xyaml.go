package util

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Server []Server `yaml:"server"`
}

type Server struct {
	Ip   string `yaml:"ip"`
	Port int    `yaml:"port"`
	User string `yaml:"user"`
	Psw  string `yaml:"psw"`
}

// read yaml config
// 注：path为yaml或yml文件的路径
func ReadYamlConfig(path string) (*Config, error) {
	conf := &Config{}
	if f, err := os.Open(path); err != nil {
		return nil, err
	} else {
		yaml.NewDecoder(f).Decode(conf)
	}
	return conf, nil
}
