# eco_shell

#### 介绍

批量执行shell命令

#### 使用

- 初始化

````
- go mod init eco/shell
- go mod tidy
````

- server.yaml文件，编辑节点信息
- 具体操作查看帮助信息

````
- go run main.go help
````

![img.png](img.png)

````
- go run main.go cmd "free -h"
````

![img_1.png](img_1.png)

````
- go run main.go scp zfb.jpg /data/zfb.jpg
````

![img_2.png](img_2.png)

- 如果没有go环境，可使用build文件夹中编译后的shell

````
- ./shell help
````

#### 作者

lee

#### 捐助

如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。
![](zfb.jpg)

